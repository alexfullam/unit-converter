#!/bin/python3
# Run this file to start the program

# Flask library - web development
# render_template: allows html files to be rendered
# request: information on the request, like get and post, and form information
from flask import render_template, request
from flask import Flask
# file that handles all conversion operations
import conversion

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    if request.method == "GET":
        return render_template('index.html')


@app.route('/length/', methods=['POST', 'GET'])
def length():
    if request.method == "GET":
        return render_template('length.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        try:
            # try to get value of form
            value_to_convert = float(request.form.get("value_to_convert"))
        except ValueError:  # e.g. they entered a letter
            return render_template('length.html', invalid_input_warning=True)

        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversion.Converter(
            "length", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_output returns 2 things: the output, and the warnings that came with it.
        warnings = converter.return_output()[1]

        # output: the output of the conversion; warnings: all warnings produced from conversion.py; method: used in base.html to decide wheather extra html for output and warnings is required
        return render_template('length.html', output=output, warnings=warnings, method=request.method)


@app.route('/mass/', methods=['POST', 'GET'])
def mass():
    if request.method == "GET":
        return render_template('mass.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        try:
            value_to_convert = float(request.form.get("value_to_convert"))
        except ValueError:
            return render_template('mass.html', invalid_input_warning=True)
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversion.Converter(
            "mass", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('mass.html', output=output, warnings=warnings, method=request.method)


@app.route('/volume/', methods=['POST', 'GET'])
def volume():
    if request.method == "GET":
        return render_template('volume.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        try:
            value_to_convert = float(request.form.get("value_to_convert"))
        except ValueError:
            return render_template('volume.html', invalid_input_warning=True)
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversion.Converter(
            "volume", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('volume.html', output=output, warnings=warnings, method=request.method)


@app.route('/temperature/', methods=['POST', 'GET'])
def temperature():
    if request.method == "GET":
        return render_template('temperature.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        try:
            value_to_convert = float(request.form.get("value_to_convert"))
        except ValueError:
            return render_template('temperature.html', invalid_input_warning=True)
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversion.Converter(
            "temperature", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('temperature.html', output=output, warnings=warnings, method=request.method)


@app.route('/time/', methods=['POST', 'GET'])
def time():
    if request.method == "GET":
        return render_template('time.html')
    elif request.method == "POST":
        unit_to_convert = request.form.get("unit_to_convert")
        try:
            value_to_convert = float(request.form.get("value_to_convert"))
        except ValueError:
            return render_template('time.html', invalid_input_warning=True)
        unit_to_convert_to = request.form.get("unit_to_convert_to")

        converter = conversion.Converter(
            "time", unit_to_convert, unit_to_convert_to, value_to_convert)
        converter.set_base()
        output = converter.return_output()[0]
        # return_ouput returns 2 things: the output, and the warningts that came with it.
        warnings = converter.return_output()[1]

        return render_template('time.html', output=output, warnings=warnings, method=request.method)


if __name__ == '__main__':
    app.run()
    # app.run(debug=True) #for debug
